#ifndef PERSISTANCE_H
#define PERSISTANCE_H

#include <QVector>
#include <utility>
#include <boost/config.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

#include "unionfind.h"

/**
 * @brief Propriétées liées aux sommets du graphe
 */
struct VertexProperties
{
    /**
     * @brief Retiens les paires de sommets de la courbe originale
     */
    std::pair<unsigned, unsigned> vertices;
    /**
     * @brief Distance entre les deux sommets de la courbe originale
     */
    float poids;
    VertexProperties() : vertices(std::make_pair(0,0)), poids(0) {}
    VertexProperties(unsigned f, unsigned s, float p) : vertices(std::make_pair(f,s)), poids(p) {}
};

/**
 * @brief Type de graphe définis.
 * @value Le graphe est définis par
 *          - Une liste de sommets
            - Pour chaque sommet, on a un vecteur pour les arètes
            - Le graphe est non orienté
            - Chaque sommet utilise les propriétés de @VertexProperties
 */
typedef boost::adjacency_list <
 boost::listS, boost::vecS, boost::undirectedS,
 VertexProperties
> Graph;

/**
 * @brief Classe permettant le calcul de la persistance homologique à partir d'un graphe @Graph
 */
class Persistance
{
private:
    Graph graph_;

public:
    Persistance(Graph g);

    QVector<unsigned> tri_poids_graph(Graph g);
    /**
     * @brief Trie les éléments d'une map
     * @param mamap la map à trier, ses éléments contiennent un entier non signé et un flottant
     * @return un vecteur d'entiers non signés,
     */
    QVector<unsigned> tri_map(std::map <unsigned,float> mamap);

    QVector<unsigned> tri_vec(QVector<std::pair<unsigned,float>> vec);
    /**
     * @brief Calcul de la persistance
     * @return une paire d'entiers non signés, les identifiants des deux sommets du graphe pour laquelle la distance est maximale
     */
    std::pair<unsigned,unsigned> persistance();

    /**
     * @brief graph
     * @param i : numéro du sommet recherché
     * @return Propriétés du sommet i du graph @Graph
     */
    VertexProperties graph(unsigned i) const { return graph_[i]; }

    /**
     * @brief valeurPersistance
     * @param i : numéro du premier sommet
     * @param j : numéro du second sommet
     * @return Différence entre les poids des deux sommets
     */
    float valeurPersistance(unsigned i, unsigned j) const { return std::abs(graph_[j].poids-graph_[i].poids); }

    void setGraph(const Graph& g) { graph_ = g; }
};

#endif // PERSISTANCE_H
