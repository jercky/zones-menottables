#ifndef UNIONFINDNOEUD_H
#define UNIONFINDNOEUD_H

#include <QDebug>

/**
 * @brief Représente un élément de la structure Union Find
 */
template<typename T>
class UnionFindNoeud
{
private:
    int rang;
    UnionFindNoeud* parent;
    char nom;
    T e;
public:
    UnionFindNoeud(T _e, unsigned r);
    void make_set(T _e, unsigned r);
    UnionFindNoeud* find_set();
    void union_set(UnionFindNoeud element);
    void afficherArbre();
    inline void setName(char c){nom = c;}
    inline void setElement(T _e){e = _e;}
    inline T getElement(){return e;}
};

template<typename T>
UnionFindNoeud<T>::UnionFindNoeud(T _e, unsigned r)
    : parent(this), rang(r), e(_e)
{
}

template<typename T>
void UnionFindNoeud<T>::make_set(T _e, unsigned r)
{
    parent = this;
    rang = r;
    e = _e;
}

template<typename T>
UnionFindNoeud<T>* UnionFindNoeud<T>::find_set()
{
    if (this->parent != this)
        return this->parent->find_set();
    return this->parent;
}

template<typename T>
void UnionFindNoeud<T>::union_set(UnionFindNoeud<T> element)
{
    UnionFindNoeud<T>* xRacine = find_set();
    UnionFindNoeud<T>* yRacine = element.find_set();
    if (xRacine != yRacine)
    {
        if (xRacine->rang > yRacine->rang)
            xRacine->parent = yRacine;
        else
        {
            yRacine->parent = xRacine;
        }
    }
}

template<typename T>
void UnionFindNoeud<T>::afficherArbre()
{
    if (this->parent != this)
        parent->afficherArbre();
    qDebug() << rang << nom;
}

#endif // UNIONFINDNOEUD_H
