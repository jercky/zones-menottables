#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ChenHanImproved/BaseModel.h"
#include "ChenHanImproved/RichModel.h"
#include "ChenHanImproved/ExactMethodForDGP.h"
#include "ChenHanImproved/PreviousCH.h"
#include "ChenHanImproved/ImprovedCHWithFilteringRule.h"
#include "ChenHanImproved/XinWangImprovedCH.h"

 #include <QTimer>
#include <boost/pending/disjoint_sets.hpp>

// exemple pour charger un fichier .obj
void MainWindow::on_pushButton_chargement_clicked()
{
    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));

    // chargement du fichier .obj dans la variable globale "mesh"
    OpenMesh::IO::read_mesh(mesh, fileName.toUtf8().constData());

    mesh.update_normals();

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // Debut ouverture du modele avec CHI
    model = new CRichModel(fileName.toUtf8().data());
    try
    {
        model->LoadModel();
    }
    catch(const char* msg)
    {
        qDebug() << "ERRORS happen!\n" << msg;
    }
    model->Preprocess();
    if (!model->HasBeenLoad() || !model->HasBeenProcessed())
    {
        qDebug() << "The model fails to be handled.";
    }
    // Fin ouverture du modele avec CHI

    // on affiche le maillage
    displayMesh(&mesh);
    ori_mesh = mesh;

    ui->spinBox_depart->setRange(0,mesh.n_vertices());
    ui->spinBox_arrivee->setRange(0,mesh.n_vertices());
}

void MainWindow::cacherFaces(MyMesh *_mesh, MyMesh::Point cam)
{
    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
   {
       FaceHandle fh = *curFace;
       float sign = _mesh->normal(fh) | cam;
       if (sign >= 0)
           _mesh->set_color(fh,{255,0,0}); // Rouge pour les faces cachées
       else
           _mesh->set_color(fh,{0,0,255}); // Bleu pour les faces de devant
   }
}

MyMesh::Point MainWindow::camera()
{
    const float theta = ui->doubleSpinBox_silX->value() * M_PI; // multiplie par pi car la valeur est dans [0,1] au lieu de [0, pi]
    const float phi   = ui->doubleSpinBox_silY->value() * 2 * M_PI; // multiplie par 2*pi car la valeur est dans [0,1] au lieu de [0, 2*pi]

    //const float theta = ui->displayWidget->camera_point_3D[0] * M_PI; // multiplie par pi car la valeur est dans [0,1] au lieu de [0, pi]
    //const float phi   = ui->displayWidget->camera_point_3D[1] * 2 * M_PI;
    //qDebug() <<"Camera: "<< theta << phi;
    const float x = sinf(theta)*cosf(phi);
    const float y = sinf(theta)*sinf(phi);
    const float z = cosf(theta);
    //qDebug()<<"x: "<<x<<" y: "<<y<<" z: "<<z<<endl;
    return MyMesh::Point(x, y, z);
 }

void MainWindow::pourcentageFaceRouge(MyMesh *_mesh){
    //afficher pourcentage de faces bleues ou rouges
    int nbFaceTotal = _mesh->n_faces();
    int nbFaceRouge=0;
    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        MyMesh::Color  couleur = _mesh->color(curFace);
        if(couleur[0]==255)
            nbFaceRouge++;
    }
    qDebug()<<"nbfaceRouge: "<< nbFaceRouge;
    qDebug()<<"nbfaceTotale: "<< nbFaceTotal;

    float pourcentageFaceRouge = (nbFaceRouge*100)/nbFaceTotal;
    qDebug()<<"pourcentage face rouge"<< pourcentageFaceRouge;
//    ui->lineEdit->setText(QString::number(pourcentageFaceRouge));

    //afficher le point qui correspond à la camera
    MyMesh::Point pts= camera();
    //MyMesh::Point pts= ui->displayWidget->camera_point_3D; // MODIF
    //qDebug() <<"PFaceRouges: "<< pts[0] << pts[1] << pts[2];
    VertexHandle vhi=_mesh->add_vertex(pts);
    _mesh->data(vhi).thickness = 88;
    if(pourcentageFaceRouge>=80)
        _mesh->set_color(vhi,MyMesh::Color(0, 0,255));
    if(pourcentageFaceRouge>50&&pourcentageFaceRouge<80)
        _mesh->set_color(vhi,MyMesh::Color(0, 255,255));
    if(pourcentageFaceRouge>30 && pourcentageFaceRouge<=50)
        _mesh->set_color(vhi,MyMesh::Color(255, 80,255));
    if(pourcentageFaceRouge<30)
        _mesh->set_color(vhi,MyMesh::Color(80, 255,255));
    if(pourcentageFaceRouge>46 && pourcentageFaceRouge<=49)
        _mesh->set_color(vhi,MyMesh::Color(50, 80,255));
}

void MainWindow::on_pushButton_creerImage_clicked()
{
   //afficher le bunny automatiquement
    // fenêtre de sélection des fichiers
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Mesh"), "", tr("Mesh Files (*.obj)"));


    // chargement du fichier .obj dans la variable globale "mesh"
    OpenMesh::IO::read_mesh(mesh, fileName.toUtf8().constData());

    mesh.update_normals();

    // initialisation des couleurs et épaisseurs (sommets et arêtes) du mesh
    resetAllColorsAndThickness(&mesh);

    // Debut ouverture du modele avec CHI
    model = new CRichModel(fileName.toUtf8().data());
    try
    {
        model->LoadModel();
    }
    catch(const char* msg)
    {
        qDebug() << "ERRORS happen!\n" << msg;
    }
    model->Preprocess();
    if (!model->HasBeenLoad() || !model->HasBeenProcessed())
    {
        qDebug() << "The model fails to be handled.";
    }
    // Fin ouverture du modele avec CHI

    // on affiche le maillage
    displayMesh(&mesh);
    ori_mesh = mesh;
// ******************************************************

    //dimension de l'image via coordonnees X,Y
    float pas=0.01f;
    QImage image(1/pas, 1/pas, QImage::Format_RGB32);
    QRgb value = qRgb(12, 17, 136);
    // ** test affichage image en rouge : OK **
    /*QRgb value = qRgb(255, 0, 0);

    for(int x=0; x < image.width();x++){
        for(int y=0; y<image.height();y++){
            image.setPixel(x,y,value);
        }
    }*/
    int pourcentMin=100;
    int pourcentMax=0;

    int imX = 0;
    for(float x=0;x<1;x+=pas)
    {
        int imY = 0;
        for(float y=0;y<1;y+=pas)
        {
            //calcul vecteur unitaire
            const float theta = x * M_PI; // multiplie par pi car la valeur est dans [0,1] au lieu de [0, pi]
            const float phi   = y * 2 * M_PI;
            const float vecteurX = sinf(theta)*cosf(phi);
            const float vecteurY = sinf(theta)*sinf(phi);
            const float vecteurZ = cosf(theta);
           // qDebug()<<"x: "<<vecteurX<<" y: "<<vecteurY<<" z: "<<vecteurZ<<endl;
            MyMesh::Point camera = MyMesh::Point(vecteurX,vecteurY,vecteurZ);

            //calculer faces rouges et bleues
            mesh = ori_mesh;
            cacherFaces(&mesh,camera);
            //calculer nb faces rouges depuis chaque Vunitaire
            int nbFaceTotal = mesh.n_faces();
            int nbFaceRouge=0;
            for (MyMesh::FaceIter curFace = mesh.faces_begin(); curFace != mesh.faces_end(); curFace++)
            {
                MyMesh::Color  couleur = mesh.color(curFace);
                if(couleur[0]==255)
                    nbFaceRouge++;
            }
            qDebug()<<"nbfaceRouge: "<< nbFaceRouge;
            qDebug()<<"nbfaceTotale: "<< nbFaceTotal;

            float pourcentageFaceRouge = (nbFaceRouge*100)/nbFaceTotal;
            qDebug()<<"pourcentage face rouge pour ("<<QString::number(x, 'f', 2)<<","<<QString::number(y, 'f', 2)<<")"<<": "<< pourcentageFaceRouge;

            //trouver le max et min de pourcentage
            if(pourcentageFaceRouge>pourcentMax)
                pourcentMax=pourcentageFaceRouge;
            if(pourcentageFaceRouge<pourcentMin)
                pourcentMin=pourcentageFaceRouge;

            //gradient de couleur
            if(pourcentageFaceRouge<45)
                 value = qRgb(12, 17, 136);
            if(pourcentageFaceRouge>=45 && pourcentageFaceRouge<48)
                 value = qRgb(84, 14, 95);
            if(pourcentageFaceRouge>=48 && pourcentageFaceRouge<51)
                 value = qRgb(157, 12, 54);
            if(pourcentageFaceRouge>=51 && pourcentageFaceRouge<54)
                 value = qRgb(206, 10, 27);
            if(pourcentageFaceRouge>=54)
                 value = qRgb(255, 0, 0);

            image.setPixel(imX,imY,value);
            imY++;
        }
        imX++;
        imY=0;
    }
    qDebug()<<"pourcentage min: "<<pourcentMin<<endl<<"pourcentage max: "<<pourcentMax<<endl;
    QString txt= QString("Pourcentage Max: %1").arg(QString::number(pourcentMax));
    ui->label_PMax->setText(txt);
    QString txt2= QString("Pourcentage Min: %1").arg(QString::number(pourcentMin));
    ui->label_PMin->setText(txt2);


    //taille label & affichage image
    ui->label_img->show();
    ui->label_PMax->show();
    ui->label_PMin->show();
    ui->label_img->resize(QPixmap::fromImage(image).size());
    ui->label_img->setPixmap(QPixmap::fromImage(image).scaled(200,200,Qt::IgnoreAspectRatio,Qt::FastTransformation));
    ui->label_img->show();

}


float MainWindow::distance(VertexHandle vh1, VertexHandle vh2)
{
    MyMesh::Point p1 = mesh.point(vh1);
    MyMesh::Point p2 = mesh.point(vh2);
    return sqrtf(powf(p2[0]-p1[0],2)+powf(p2[1]-p1[1],2)+powf(p2[2]-p1[2],2));
}

float MainWindow::distance(CPoint3D p1, CPoint3D p2)
{
    return sqrtf(powf(p2.x-p1.x,2)+powf(p2.y-p1.y,2)+powf(p2.z-p1.z,2));
}

int binomialCoef(int n, int k) {
    double res = 1;
    for (int i = 1; i <= k; ++i)
        res = res * (n - k + i) / i;
    return (int)(res + 0.01);
}

bool MainWindow::areVerticesNeighbors(VertexHandle vh1, VertexHandle vh2)
{
    bool neighbors = false;
    HalfedgeHandle heh_o = mesh.halfedge_handle(vh1);
    HalfedgeHandle heh = heh_o;

    do
    {
        if (mesh.to_vertex_handle(heh) == vh2) neighbors = true;
        heh = mesh.next_halfedge_handle(mesh.opposite_halfedge_handle(heh));
    } while(heh != heh_o);

    return neighbors;
}

Graph MainWindow::toGraph()
{
    Graph g;
    typedef boost::graph_traits<Graph>::vertex_descriptor vertex_t;
    QVector<vertex_t> vertices;

    unsigned pos = 0 , p = 0;
    for (auto& courbe : courbes)
    {
        // Ajoute sommets
        for (int i = 0 ; i < courbe.length() ; ++i)
        {
            VertexHandle vh1 = courbe[i];

            for (int j = 0 ; j < i ; ++j)
            {
                VertexHandle vh2 = courbe[j];

                vertex_t v = add_vertex(g);
                g[v].vertices = make_pair(vh1.idx(), vh2.idx());
                g[v].poids = distance(vh1,vh2);
                vertices.push_back(v);

            }
        }

        // Ajoute arètes
        int coeff = binomialCoef(courbe.length(),2);
        for (int i = 0 ; i < courbe.length() ; ++i)
        {
            VertexHandle vh1 = courbe[i];

            for (int j = 0 ; j < i ; ++j)
            {
                VertexHandle vh2 = courbe[j];

                if (vh1 != vh2)
                {
                    unsigned im1, jp1;
                    if (j > 0) im1 = p-1;
                    else       im1 = pos+coeff-(courbe.length()-min(i+1,courbe.length()-1));

                    if (i < courbe.length()-1) jp1 = (p-pos+i)%coeff+pos;
                    else                       jp1 = pos+binomialCoef(j+1,2)-j;

                    add_edge(vertices[im1],vertices[p],g);
                    add_edge(vertices[jp1],vertices[p],g);

                    if (i-j != 1 && coeff > 3)
                    {
                        unsigned ip1, jm1;
                        ip1 = p+1;
                        jm1 = p-(i-1);

                        add_edge(vertices[ip1],vertices[p],g);
                        add_edge(vertices[jm1],vertices[p],g);
                    }
                    p++;
                }

            }
        }

        pos = p;
    }

//    for(auto vd : boost::make_iterator_range(boost::vertices(g)))
//        qDebug() << vd << g[vd].vertices << g[vd].poids;

    return g;
}

void MainWindow::on_pushButton_silhouette_clicked()
{
    MyMesh::Point cam = camera();

    mesh = ori_mesh;
    cacherFaces(&mesh,cam);

    displayMesh(&mesh);
}

bool MainWindow::estDeContour(MyMesh *_mesh, EdgeHandle eh)
{
    FaceHandle f0 = _mesh->face_handle(_mesh->halfedge_handle(eh,0));
    FaceHandle f1 = _mesh->face_handle(_mesh->halfedge_handle(eh,1));
    if (!f1.is_valid())
        f1 = f0;
    return (_mesh->color(f0) != _mesh->color(f1) || (f0 == f1 && _mesh->color(f0) == MyMesh::Color(0,0,255)));
}

void MainWindow::contour(MyMesh *_mesh)
{
    courbes.clear();
    bool* tab_passage = new bool[_mesh->n_edges()];
    for (unsigned i = 0 ; i < _mesh->n_edges() ; ++i)
        tab_passage[i] = true;

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        EdgeHandle eh = *curEdge;
        if (tab_passage[eh.idx()])
        {
            if (estDeContour(_mesh, eh))
                marquerContour(_mesh,eh,tab_passage);
            else
                tab_passage[eh.idx()] = false;
        }
    }

    delete[] tab_passage;
}

void MainWindow::marquerArete(MyMesh *_mesh, EdgeHandle eh)
{
    _mesh->set_color(eh,{0,255,0});
    _mesh->data(eh).thickness = 2;
}

void MainWindow::marquerContour(MyMesh *_mesh, EdgeHandle start_eh, bool* tab_passage)
{
    QVector<VertexHandle> courbe;
    HalfedgeHandle heh = _mesh->halfedge_handle(start_eh,0);

    marquerArete(_mesh, start_eh);
    tab_passage[start_eh.idx()] = false;

    courbe.push_back(_mesh->from_vertex_handle(heh));
    heh = _mesh->next_halfedge_handle(heh);
    EdgeHandle eh = _mesh->edge_handle(heh);

    while (eh != start_eh)
    {
        if (estDeContour(_mesh, eh) && tab_passage[eh.idx()])
        {
            courbe.push_back(_mesh->from_vertex_handle(heh));
            marquerArete(_mesh, eh);
            tab_passage[eh.idx()] = false;
        }
        else
            heh = _mesh->opposite_halfedge_handle(heh);

        heh = _mesh->next_halfedge_handle(heh);
        eh = _mesh->edge_handle(heh);
    }

    courbes.push_back(courbe);
}

void MainWindow::afficherCourbes()
{
    for (auto& courbe : courbes)
    {
        for (auto& vh : courbe)
        {
            qDebug() << vh.idx();
        }
        qDebug() << courbe.length() << endl;
    }
    qDebug() << courbes.length();
}

void MainWindow::on_pushButton_contour_clicked()
{
    resetAllColorsAndThickness(&mesh);
    MyMesh::Point cam = camera();

    mesh = ori_mesh;
    cacherFaces(&mesh,cam);
    contour(&mesh);

    displayMesh(&mesh);
}

 MyMesh::Point MainWindow::creerCamera(float x, float y)
 {
    const float theta = x * M_PI; // multiplie par pi car la valeur est dans [0,1] au lieu de [0, pi]
    const float phi   = y * 2 * M_PI;
    const float vecteurX = sinf(theta)*cosf(phi);
    const float vecteurY = sinf(theta)*sinf(phi);
    const float vecteurZ = cosf(theta);
    MyMesh::Point camera = MyMesh::Point(vecteurX,vecteurY,vecteurZ);
    return camera;
}

ResultatPersistance MainWindow::persistance3DAll(float pas)
{
    qDebug()<<"Debut peristance3D"<<endl;

    ResultatPersistance r;
    r.valeur = 0;
    ui->progressBar->show();

    for(float x=0;x<=1;x+=pas)
            for(float y=0;y<=1;y+=pas)
            {
                //creer chaque vecteur unitaire(camera) à chaque incrementation
                qDebug()<<"x: "<<x<<", "<<"y: "<<y;
                ResultatPersistance resultat = persistance2D(x,y);
                if (resultat.valeur > r.valeur)
                {
                    r = resultat;
                    r.cam_x = x;
                    r.cam_y = y;
                    marquerPersistance(r);
                }
                ui->progressBar->setValue(1/(1/(x)+1)*100);
            }

    ui->progressBar->hide();
    ui->progressBar->setValue(0);
    return r;

}

ResultatPersistance MainWindow::persistance3DVoisins(float pas)
{
    int max = 10;
    boost::random::mt19937 rng(time(NULL));
    boost::random::uniform_01<boost::random::mt19937> dist(rng);
    ResultatPersistance r = persistance2D(0,0);
    ui->progressBar->show();

    for (int i = 0 ; i < max ; ++i)
    {
        float x = dist();
        float y = dist();
        r = persistance3DVoisinsDepuis(x,y,pas,r);
//        qDebug() << "Pourcentage :" << float(i)/max*100 << '(' << x << ';' << y << ')';
        ui->progressBar->setValue(float(i)/max*100);
    }

    marquerPersistance(r);
    ui->progressBar->hide();
    ui->progressBar->setValue(0);
    return r;
}

ResultatPersistance MainWindow::persistance3DVoisinsDepuis(float x, float y, float pas, ResultatPersistance resultat_cour)
{
    bool max_trouve = false;

    {
        ResultatPersistance r = persistance2D(x,y);
        if (r.valeur > resultat_cour.valeur)
        {
            resultat_cour = r;
            marquerPersistance(resultat_cour);
        }
    }

    while (!max_trouve)
    {
        max_trouve = true;

        QVector<ResultatPersistance> vec_res;
        // Prend les couples (x',y') dans le demi-masque supérieur autour de (x,y)
        if (x+pas < 1) vec_res.push_back(persistance2D(x+pas,y));
        if (y+pas < 1) vec_res.push_back(persistance2D(x,y+pas));
        if (x+pas < 1 && y+pas < 1) vec_res.push_back(persistance2D(x+pas,y+pas));
        if (x-pas > 0 && y+pas < 1) vec_res.push_back(persistance2D(x-pas,y+pas));

//        if (x-pas > 0) vec_res.push_back(persistance2D(x-pas,y));
//        if (y-pas > 0) vec_res.push_back(persistance2D(x,y-pas));
//        if (x-pas > 0 && y-pas > 0) vec_res.push_back(persistance2D(x-pas,y-pas));
//        if (x+pas < 1 && y-pas > 0) vec_res.push_back(persistance2D(x+pas,y-pas));

        for (auto& r : vec_res)
            if (r.valeur > resultat_cour.valeur)
            {
                resultat_cour = r;
                marquerPersistance(resultat_cour);
                max_trouve = false;
            }

        if (vec_res.isEmpty()) max_trouve = true;
    }

    return resultat_cour;
}

ResultatPersistance MainWindow::persistance2D(float x, float y)
{
    MyMesh::Point camera = creerCamera(x,y);

    mesh = ori_mesh;
    cacherFaces(&mesh,camera);
    contour(&mesh);
    Graph g = toGraph();
    Persistance persistance(g);
    pair<unsigned, unsigned> p = persistance.persistance();

    ResultatPersistance r = {x, y, g[p.first].vertices, g[p.second].vertices, persistance.valeurPersistance(p.first,p.second)};

    return r;
}

void MainWindow::on_pushButton_persistance3D_opti_clicked()
{
    resetAllColorsAndThickness(&mesh);
    ResultatPersistance resultat = persistance3DVoisins(ui->doubleSpinBox_pas->value());
    mettreSpinAJour(resultat);
}

void MainWindow::on_pushButton_persistance_clicked()
{
    resetAllColorsAndThickness(&mesh);
    ResultatPersistance r = persistance2D(ui->doubleSpinBox_silX->value(),
                                          ui->doubleSpinBox_silY->value());

    marquerPairesSommets(r.paire1,r.paire2);
    mettreSpinAJour(r);
    displayMesh(&mesh);
}

void MainWindow::on_pushButton_persistance3D_tout_clicked()
{
    resetAllColorsAndThickness(&mesh);
    ResultatPersistance resultat = persistance3DAll(ui->doubleSpinBox_pas->value());
    mettreSpinAJour(resultat);
}

void MainWindow::mettreSpinAJour(ResultatPersistance resultat)
{
    ui->spinBox_depart->setValue(resultat.paire1.first);
    ui->spinBox_arrivee->setValue(resultat.paire1.second);
}

void MainWindow::calculGeodesiques(int indexV1, int indexV2)
{
    resetAllColorsAndThickness(&mesh);
    if(indexV1 < 0 || indexV1 >= mesh.n_faces()){
        qDebug() << "f1 out of bound !";
        qDebug() << "max =" << mesh.n_faces();
    }else{
        VertexHandle fh = mesh.vertex_handle(indexV1);
        mesh.set_color(fh, MyMesh::Color(255, 255, 0));
        pointStart = mesh.point(fh);
        mesh.data(fh).thickness = 10;
    }
    if( indexV2 < 0 || indexV2 >= mesh.n_faces()){
        qDebug() << "f2 out of bound !";
        qDebug() << "max =" << mesh.n_faces();
    }
    else{
        VertexHandle fh = mesh.vertex_handle(indexV2);
        mesh.set_color(fh, MyMesh::Color(255, 255, 0));
        pointEnd = mesh.point(fh);
        mesh.data(fh).thickness = 10;
    }

    // Debut utilisation CHI
    CExactMethodForDGP * algorithm = new CXinWangImprovedCH(*model, indexV1);
    algorithm->Execute();
    vector<CPoint3D> resultpoints;
    if (!(indexV2 < 0 || indexV2 >=  model->GetNumOfVerts()))
    {
        algorithm->BackTrace(indexV2, resultpoints);
    }
    // Fin utilisation CHI
    // Prendre le sommet C mileu de la geodesique entre A et B
    // Calculer C' le symetrique de C par rapport a la droite AB
    // Trouver D le sommet le plus proche du maillage de C'
    // Afficher la geodesique BD puis DA

    // Calcul sommet C mileu de la geodesique entre A et B
    float total = 0.0f;
    for (int i = 1; i < resultpoints.size(); i++)
        total += distance(resultpoints[i-1], resultpoints[i]);
    total /= 2.0f;
    int reshalf;
    float tempTotal = 0.0f;
    for (reshalf = 1; tempTotal < total; reshalf++)
    {
        tempTotal += distance(resultpoints[reshalf-1], resultpoints[reshalf]);
    }
    //reshalf = resultpoints.size()/2;
    MyMesh::Point ptC (resultpoints[reshalf].x, resultpoints[reshalf].y, resultpoints[reshalf].z);

    MyMesh::Point M ((resultpoints[0].x+resultpoints[resultpoints.size()-1].x)/2.0f,
            (resultpoints[0].y+resultpoints[resultpoints.size()-1].y)/2.0f,
            (resultpoints[0].z+resultpoints[resultpoints.size()-1].z)/2.0f);
    ptC = M+M-ptC; // Calculer C' le symetrique de C par rapport a la droite AB

    // Trouver D le sommet le plus proche du maillage de C'
    VertexHandle ptD = mesh.vertices_begin();
    float distMin = 99999.9f;
    for (MyMesh::VertexIter curVert = mesh.vertices_begin(); curVert != mesh.vertices_end(); curVert++)
    {
        VertexHandle vh = *curVert;
        float distance = (ptC[0] - mesh.point(vh)[0])*(ptC[0] - mesh.point(vh)[0]) +
                (ptC[1] - mesh.point(vh)[1])*(ptC[1] - mesh.point(vh)[1]) +
                (ptC[2] - mesh.point(vh)[2])*(ptC[2] - mesh.point(vh)[2]);
        distance = sqrt (distance);
        if (distance < distMin)
        {
            ptD = vh;
            distMin = distance;
        }
    }
    // Afficher la geodesique BD puis DA
    vector<CPoint3D> newresultpoints;
    algorithm = new CXinWangImprovedCH(*model, indexV1);
    algorithm->Execute();
    if (!(indexV1 < 0 || indexV1 >=  model->GetNumOfVerts()))
    {
        algorithm->BackTrace(ptD.idx(), newresultpoints);
        while (!newresultpoints.empty())
        {
            resultpoints.push_back(newresultpoints.back());
            newresultpoints.pop_back();
        }
    }
    algorithm = new CXinWangImprovedCH(*model, ptD.idx());
    algorithm->Execute();
    if (!(ptD.idx() < 0 || ptD.idx() >=  model->GetNumOfVerts()))
    {
        algorithm->BackTrace(indexV2, newresultpoints);
        while (!newresultpoints.empty())
        {
            resultpoints.push_back(newresultpoints.back());
            newresultpoints.pop_back();
        }
    }
    // TEST affichage symetrique
    //resultpoints.push_back(CPoint3D(ptC[0], ptC[1], ptC[2]));

    // Affichage chemin obtenu avec CHI, a faire apres displayMesh
    displayPath(resultpoints);
}

void MainWindow::on_pushButton_chemin_clicked()
{
    // on récupère les sommets de départ et d'arrivée
    int indexV1 = ui->spinBox_depart->value();
    int indexV2 = ui->spinBox_arrivee->value();

    calculGeodesiques(indexV1, indexV2);

    displayMesh(&mesh);
}

void MainWindow::marquerSommet(VertexHandle vh, MyMesh::Color c)
{
    mesh.data(vh).thickness = 10;
    mesh.set_color(vh, c);
}

void MainWindow::marquerPairesSommets(pair<unsigned int, unsigned int> paire1, pair<unsigned int, unsigned int> paire2)
{
    marquerSommet(mesh.vertex_handle(paire1.first), MyMesh::Color(255,255,0));
    marquerSommet(mesh.vertex_handle(paire1.second), MyMesh::Color(255,255,0));

    if (paire2.first == paire1.first || paire2.first == paire1.second)
        marquerSommet(mesh.vertex_handle(paire2.first), MyMesh::Color(255,0,0));
    else
        marquerSommet(mesh.vertex_handle(paire2.first), MyMesh::Color(255,0,255));

    if (paire2.second == paire1.first || paire2.second == paire1.second)
        marquerSommet(mesh.vertex_handle(paire2.second), MyMesh::Color(255,0,0));
    else
        marquerSommet(mesh.vertex_handle(paire2.second), MyMesh::Color(255,0,255));

    qDebug() << '(' << paire1.first << ';' << paire1.second << ')'
             << '(' << paire2.first << ';' << paire2.second << ')';
}

void MainWindow::marquerPersistance(ResultatPersistance r)
{
    resetAllColorsAndThickness(&mesh);
    MyMesh::Point camera=creerCamera(r.cam_x,r.cam_y);
    cacherFaces(&mesh,camera);
    contour(&mesh);
    marquerPairesSommets(r.paire1,r.paire2);
    displayMesh(&mesh);
}

/* **** fonctions supplémentaires **** */
// permet d'initialiser les couleurs et les épaisseurs des élements du maillage
void MainWindow::resetAllColorsAndThickness(MyMesh* _mesh)
{
    for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
    {
        _mesh->data(*curVert).thickness = 1;
        _mesh->set_color(*curVert, MyMesh::Color(0, 0, 0));
    }

    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        _mesh->set_color(*curFace, MyMesh::Color(150, 150, 150));
    }

    for (MyMesh::EdgeIter curEdge = _mesh->edges_begin(); curEdge != _mesh->edges_end(); curEdge++)
    {
        _mesh->data(*curEdge).thickness = 1;
        _mesh->set_color(*curEdge, MyMesh::Color(0, 0, 0));
    }
}

// charge un objet MyMesh dans l'environnement OpenGL
void MainWindow::displayMesh(MyMesh* _mesh, bool isTemperatureMap, float mapRange)
{
    unsigned displayed_faces = 0;
    for (MyMesh::FaceIter curFace = _mesh->faces_begin(); curFace != _mesh->faces_end(); curFace++)
    {
        if (_mesh->color(*curFace) != MyMesh::Color(0,255,0))
            displayed_faces++;
    }

    GLuint* triIndiceArray = new GLuint[displayed_faces * 3];
    GLfloat* triCols = new GLfloat[displayed_faces * 3 * 3];
    GLfloat* triVerts = new GLfloat[displayed_faces * 3 * 3];

    int i = 0;

    if(isTemperatureMap)
    {
        QVector<float> values;

        if(mapRange == -1)
        {
            for (MyMesh::VertexIter curVert = _mesh->vertices_begin(); curVert != _mesh->vertices_end(); curVert++)
                values.append(fabs(_mesh->data(*curVert).value));
            qSort(values);
            mapRange = values.at(values.size()*0.8);
            qDebug() << "mapRange" << mapRange;
        }

        float range = mapRange;
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;

        for (; fIt!=fEnd; ++fIt)
        {
            fvIt = _mesh->cfv_iter(*fIt);
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++; ++fvIt;
            if(_mesh->data(*fvIt).value > 0){triCols[3*i+0] = 255; triCols[3*i+1] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+2] = 255 - std::min((_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            else{triCols[3*i+2] = 255; triCols[3*i+1] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0); triCols[3*i+0] = 255 - std::min((-_mesh->data(*fvIt).value/range) * 255.0, 255.0);}
            triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
            triIndiceArray[i] = i;

            i++;
        }
    }
    else
    {
        MyMesh::ConstFaceIter fIt(_mesh->faces_begin()), fEnd(_mesh->faces_end());
        MyMesh::ConstFaceVertexIter fvIt;
        for (; fIt!=fEnd; ++fIt)
        {
            if (_mesh->color(*fIt) != MyMesh::Color(0,255,0))
            {

                fvIt = _mesh->cfv_iter(*fIt);
                triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
                triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
                triIndiceArray[i] = i;

                i++; ++fvIt;
                triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
                triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
                triIndiceArray[i] = i;

                i++; ++fvIt;
                triCols[3*i+0] = _mesh->color(*fIt)[0]; triCols[3*i+1] = _mesh->color(*fIt)[1]; triCols[3*i+2] = _mesh->color(*fIt)[2];
                triVerts[3*i+0] = _mesh->point(*fvIt)[0]; triVerts[3*i+1] = _mesh->point(*fvIt)[1]; triVerts[3*i+2] = _mesh->point(*fvIt)[2];
                triIndiceArray[i] = i;
                i++;
            }

        }
    }


    ui->displayWidget->loadMesh(triVerts, triCols, displayed_faces * 3 * 3, triIndiceArray, displayed_faces * 3);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[_mesh->n_edges() * 2];
    GLfloat* linesCols = new GLfloat[_mesh->n_edges() * 2 * 3];
    GLfloat* linesVerts = new GLfloat[_mesh->n_edges() * 2 * 3];

    i = 0;
    QHash<float, QList<int> > edgesIDbyThickness;
    for (MyMesh::EdgeIter eit = _mesh->edges_begin(); eit != _mesh->edges_end(); ++eit)
    {
        float t = _mesh->data(*eit).thickness;
        FaceHandle f0 = _mesh->face_handle(_mesh->halfedge_handle(*eit,0));
        FaceHandle f1 = _mesh->face_handle(_mesh->halfedge_handle(*eit,1));
        if(t > 0 ) // && (_mesh->color(f0) != MyMesh::Color(0,255,0) || _mesh->color(f1) != MyMesh::Color(0,255,0))
        {
            if(!edgesIDbyThickness.contains(t))
                edgesIDbyThickness[t] = QList<int>();
            edgesIDbyThickness[t].append((*eit).idx());
        }
    }
    QHashIterator<float, QList<int> > it(edgesIDbyThickness);
    QList<QPair<float, int> > edgeSizes;
    while (it.hasNext())
    {
        it.next();

        for(int e = 0; e < it.value().size(); e++)
        {
            int eidx = it.value().at(e);

            MyMesh::VertexHandle vh1 = _mesh->to_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            if (!_mesh->face_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0)).is_valid() &&
                !_mesh->face_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 1)).is_valid())
                continue;

            linesVerts[3*i+0] = _mesh->point(vh1)[0];
            linesVerts[3*i+1] = _mesh->point(vh1)[1];
            linesVerts[3*i+2] = _mesh->point(vh1)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;

            MyMesh::VertexHandle vh2 = _mesh->from_vertex_handle(_mesh->halfedge_handle(_mesh->edge_handle(eidx), 0));
            linesVerts[3*i+0] = _mesh->point(vh2)[0];
            linesVerts[3*i+1] = _mesh->point(vh2)[1];
            linesVerts[3*i+2] = _mesh->point(vh2)[2];
            linesCols[3*i+0] = _mesh->color(_mesh->edge_handle(eidx))[0];
            linesCols[3*i+1] = _mesh->color(_mesh->edge_handle(eidx))[1];
            linesCols[3*i+2] = _mesh->color(_mesh->edge_handle(eidx))[2];
            linesIndiceArray[i] = i;
            i++;
        }
        edgeSizes.append(qMakePair(it.key(), it.value().size()));
    }

    ui->displayWidget->loadLines(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

    GLuint* pointsIndiceArray = new GLuint[_mesh->n_vertices()];
    GLfloat* pointsCols = new GLfloat[_mesh->n_vertices() * 3];
    GLfloat* pointsVerts = new GLfloat[_mesh->n_vertices() * 3];

    i = 0;
    QHash<float, QList<int> > vertsIDbyThickness;
    for (MyMesh::VertexIter vit = _mesh->vertices_begin(); vit != _mesh->vertices_end(); ++vit)
    {
        float t = _mesh->data(*vit).thickness;
        if(t > 0)
        {
            if(!vertsIDbyThickness.contains(t))
                vertsIDbyThickness[t] = QList<int>();
            vertsIDbyThickness[t].append((*vit).idx());
        }
    }
    QHashIterator<float, QList<int> > vitt(vertsIDbyThickness);
    QList<QPair<float, int> > vertsSizes;

    while (vitt.hasNext())
    {
        vitt.next();

        for(int v = 0; v < vitt.value().size(); v++)
        {
            int vidx = vitt.value().at(v);

            pointsVerts[3*i+0] = _mesh->point(_mesh->vertex_handle(vidx))[0];
            pointsVerts[3*i+1] = _mesh->point(_mesh->vertex_handle(vidx))[1];
            pointsVerts[3*i+2] = _mesh->point(_mesh->vertex_handle(vidx))[2];
            pointsCols[3*i+0] = _mesh->color(_mesh->vertex_handle(vidx))[0];
            pointsCols[3*i+1] = _mesh->color(_mesh->vertex_handle(vidx))[1];
            pointsCols[3*i+2] = _mesh->color(_mesh->vertex_handle(vidx))[2];
            pointsIndiceArray[i] = i;
            i++;
        }
        vertsSizes.append(qMakePair(vitt.key(), vitt.value().size()));
    }

    ui->displayWidget->loadPoints(pointsVerts, pointsCols, i * 3, pointsIndiceArray, i, vertsSizes);

    delete[] pointsIndiceArray;
    delete[] pointsCols;
    delete[] pointsVerts;
}

// charge un chemin geodesique dans l'environnement OpenGL
void MainWindow::displayPath(vector<CPoint3D> resultpoints)
{
    GLuint* triIndiceArray = new GLuint[(resultpoints.size()+2) * 3];
    GLfloat* triCols = new GLfloat[(resultpoints.size()+2) * 3 * 3];
    GLfloat* triVerts = new GLfloat[(resultpoints.size()+2) * 3 * 3];

    QList<QPair<float, int> > vertsSizes;
    int i = 0;
    // Points de la geodesique
    // On utilise "i-1" quand on est en decalage avec le tableau resultpoints
    for (; i < resultpoints.size() ; ++i)
    {
        triCols[3*i+0] = 0; triCols[3*i+1] = 255; triCols[3*i+2] = 255;
        triVerts[3*i+0] = resultpoints[i].x; triVerts[3*i+1] = resultpoints[i].y; triVerts[3*i+2] = resultpoints[i].z;
        triIndiceArray[i] = i;
        vertsSizes.append(qMakePair(5, i));
    }
    // Couleur differente pour premier et dernier sommet
    // /!\ i vaut actuellement resultpoints.size()
    triCols[0] = 255; triCols[1] = 255; triCols[2] = 0;
    triCols[3*i+0] = 255; triCols[3*i+1] = 255; triCols[3*i+2] = 0;

    ui->displayWidget->loadPoints(triVerts, triCols, i * 3, triIndiceArray, i, vertsSizes);

    delete[] triIndiceArray;
    delete[] triCols;
    delete[] triVerts;

    GLuint* linesIndiceArray = new GLuint[(resultpoints.size()+2) * 2];
    GLfloat* linesCols = new GLfloat[(resultpoints.size()+2) * 2 * 3];
    GLfloat* linesVerts = new GLfloat[(resultpoints.size()+2) * 2 * 3];

    QList<QPair<float, int> > edgeSizes;
    i = 0;
    int j = 0;
    while ( i < resultpoints.size()*2 - 2)
    {
        linesVerts[3*i+0] = resultpoints[j].x; linesVerts[3*i+1] = resultpoints[j].y; linesVerts[3*i+2] = resultpoints[j].z;
        linesCols[3*i+0] = 0; linesCols[3*i+1] = 200; linesCols[3*i+2] = 200;
        linesIndiceArray[i] = i;
        edgeSizes.append(qMakePair(3, i));
        i++;
        j++;

        linesVerts[3*i+0] = resultpoints[j].x; linesVerts[3*i+1] = resultpoints[j].y; linesVerts[3*i+2] = resultpoints[j].z;
        linesCols[3*i+0] = 0; linesCols[3*i+1] = 200; linesCols[3*i+2] = 200;
        linesIndiceArray[i] = i;
        edgeSizes.append(qMakePair(3, i));
        i++;
    }

    ui->displayWidget->loadPath(linesVerts, linesCols, i * 3, linesIndiceArray, i, edgeSizes);

    delete[] linesIndiceArray;
    delete[] linesCols;
    delete[] linesVerts;

}

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->label_PMax->hide();
    ui->label_PMin->hide();
    ui->label_img->hide();
    ui->progressBar->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}




