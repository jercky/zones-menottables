#ifndef UNIONFIND_H
#define UNIONFIND_H

#include "unionfindnoeud.h"
#include <QList>
#include <utility>

/**
 * @brief Classe permettant de stokcer une structure Union Find pour des éléments de type T
 */
template<typename T>
class UnionFind
{
private:
    /**
     * @brief Les noeuds qui constituent la structure
     */
    std::map<T,UnionFindNoeud<T>*> noeuds_;
public:
    UnionFind();
    ~UnionFind();
    /**
     * @brief Insère un élément dans la structure
     * @param _e
     */
    void make_set(T _e);
    T find_set(T e);
    void union_set(T x, T y);
    void union_set(UnionFindNoeud<T>* x, UnionFindNoeud<T>* y);
    void afficherArbre();
    UnionFindNoeud<T>* search(T e);
};


template<typename T>
UnionFind<T>::UnionFind() {}

template<typename T>
UnionFind<T>::~UnionFind()
{
    noeuds_.clear();
}

template<typename T>
void UnionFind<T>::make_set(T _e)
{
    UnionFindNoeud<T>* n = new UnionFindNoeud<T>(_e,noeuds_.size());
    noeuds_.insert(std::make_pair(_e,n));
}

template<typename T>
T UnionFind<T>::find_set(T e)
{
    UnionFindNoeud<T>* r = search(e);
    return r->find_set()->getElement();
}

template<typename T>
void UnionFind<T>::union_set(T x, T y)
{
    UnionFindNoeud<T>* x_ = search(x);
    UnionFindNoeud<T>* y_ = search(y);
    union_set(x_,y_);
}

template<typename T>
UnionFindNoeud<T>* UnionFind<T>::search(T e)
{
    return noeuds_[e];
}

template<typename T>
void UnionFind<T>::union_set(UnionFindNoeud<T>* x, UnionFindNoeud<T>* y)
{
    x->union_set(*y);
}

template<typename T>
void UnionFind<T>::afficherArbre()
{
    qDebug() << "Affichage de l'arbre (début)";
    for (auto& n : noeuds_)
    {
        n->afficherArbre();
        qDebug() << "--";
    }
    qDebug() << "Affichage de l'arbre (fin)";
}

#endif // UNIONFIND_H
