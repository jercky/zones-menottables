# Projet de fin d'études - Détection des zones *menottables*
## Pré-requis
* Qt
* OpenMesh
* FreeGLut


## Installation
1. Installer le dossier OpenMesh dans votre repertoire de travail.
2. Si FreeGlut n'est pas installé, installez le avec la commande suivante:
    sudo apt install freeglut3-dev
3. Télécharger [l'exemple de projet d'Arnaud Polette](http://arnaud.polette.perso.luminy.univ-amu.fr/mod.zip).
4. Remplacer le dossier *starterLight* de l'exemple par le dossier du projet [Zones menottables](https://gitlab.com/jercky/zones-menottables).
