#include "persistance.h"
#include "math.h"

Persistance::Persistance(Graph g)
    :graph_(g)
{

}

QVector<unsigned> Persistance::tri_poids_graph(Graph g)
{
    QVector<std::pair<unsigned,float>> vec;

    // Converti la liste des poids du graph en QVector ...
    boost::graph_traits < boost::adjacency_list <> >::vertex_iterator i, end;

    int index = 0;
    for (std::tie(i,end) = vertices(g) ; i != end ; ++i)
    {
        boost::graph_traits<Graph>::vertex_descriptor u = *i;
        vec.push_back(std::make_pair(index++, g[u].poids));
    }

    // ... Puis utilise le tri de vecteurs
    return tri_vec(vec);
}

QVector<unsigned> Persistance::tri_map(std::map <unsigned,float> mamap){


    QVector<std::pair<unsigned,float>> vec;
    for (auto& m : mamap)
        vec.push_back(std::make_pair(m.first,m.second));

    return tri_vec(vec);
}

QVector<unsigned> Persistance::tri_vec(QVector<std::pair<unsigned,float>> vec)
{
    typedef std::function<bool(std::pair<unsigned, float>, std::pair<unsigned, float>)> Comparator;
    Comparator compFunctor =
            [](std::pair<unsigned, float> elem1 ,std::pair<unsigned, float> elem2)
            {
                return elem1.second < elem2.second;
            };

    std::sort(vec.begin(),vec.end(), compFunctor);


    QVector<unsigned> r;
    for (auto& m : vec)
        r.push_back(m.first);

    return r;
}

std::pair<unsigned,unsigned> Persistance::persistance()
{
    std::map<unsigned,float> p_map;

    QVector<unsigned> poids_trie = tri_poids_graph(graph_);
    QVector<unsigned> parents = poids_trie;
    UnionFind<unsigned> ufind;
//    boost::disjoint_sets<unsigned*,unsigned*> ufind(&poids_trie[0], &parents[0]);

    std::pair<unsigned,unsigned> max_pair_id;
    float max_poids = -1;

    QVector<std::pair<float,float>> r;
    for (int x = 0 ; x < poids_trie.size() ; ++x)
    {
        unsigned i = poids_trie[x];
        ufind.make_set(i);
    }

    for (int x = 0 ; x < poids_trie.size() ; ++x)
    {
        unsigned i = poids_trie[x];

        typename boost::graph_traits < Graph >::out_edge_iterator ei, ei_end;
        for (boost::tie(ei, ei_end) = out_edges(i, graph_); ei != ei_end; ++ei)
        {
          auto j = boost::target ( *ei, graph_ );
          if (graph_[j].poids <= graph_[i].poids)
              if (ufind.find_set(i) != ufind.find_set(j)) // find(i) != find(j)
              {
                  // Récupère n = max(f(find(i),find(j))
                  unsigned f_i = ufind.find_set(i);
                  unsigned f_j = ufind.find_set(j);
                  float n = std::max(graph_[f_i].poids,graph_[f_j].poids);
                  // Stocke poids max et ses indices
                  if (abs(n-graph_[i].poids) > max_poids)
                  {
                      max_poids = abs(n-graph_[i].poids);
                      unsigned f = ( n==graph_[f_i].poids?f_i:f_j); // f = find(i) si n = f(find(i)) ou find(j) si n = f(find(j))
                      max_pair_id = std::make_pair(f,i);
                  }
                  ufind.union_set(i,j); // merge(i,j)
                  r.push_back(std::make_pair(n,graph_[i].poids)); // enregistrer(n,f(i))
              }
        }


        //qDebug() << "Persistance : " << double(x)/poids_trie.size()*100 << '%';
    }

//    qDebug() << "Dist. max : " << max_poids << "pour"
//             << max_pair_id.first << '(' << graph_[max_pair_id.first].poids << ')'
//             << max_pair_id.second << '(' << graph_[max_pair_id.second].poids << ')';
//    int i = 0;
//    for (auto& a : r)
//    {
//        if (a.first < a.second)
//            qDebug() << a.first << a.second;
//        i++;
//    }

    return max_pair_id;

}
