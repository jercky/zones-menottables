#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include "ChenHanImproved/RichModel.h"
#include <QVector3D>
#include <QLabel>

#include "boost/random.hpp"
#include <ctime>

#include "persistance.h"

namespace Ui {
class MainWindow;
}

using namespace OpenMesh;
using namespace OpenMesh::Attributes;

struct MyTraits : public OpenMesh::DefaultTraits
{
    // use vertex normals and vertex colors
    VertexAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    // store the previous halfedge
    HalfedgeAttributes( OpenMesh::Attributes::PrevHalfedge );
    // use face normals face colors
    FaceAttributes( OpenMesh::Attributes::Normal | OpenMesh::Attributes::Color | OpenMesh::Attributes::Status);
    EdgeAttributes( OpenMesh::Attributes::Color | OpenMesh::Attributes::Status );
    // vertex thickness
    VertexTraits{float thickness; float value;};
    // edge thickness
    EdgeTraits{float thickness;};
};
typedef OpenMesh::TriMesh_ArrayKernelT<MyTraits> MyMesh;

/**
  * @brief Données de résultat de la persistance
  */
typedef struct {
    float cam_x;                    /// Coordonnée x sphérique de la caméra.
    float cam_y;                    /// Coordonnée y sphérique de la caméra.
    pair<unsigned,unsigned> paire1; /// Première paire de sommets : Meilleure courbe menottable.
    pair<unsigned,unsigned> paire2; /// Seconde paire de sommets : Point de décrochage de la zone.
    float valeur;                   /// Persistance pour les deux paires de sommets données.
} ResultatPersistance;

/**
 * @brief Classe principale de l'application
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief Création de la fenêtre
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    /**
     * @brief Affiche le maillage. Ecrit par Arnaud Polette.
     * @param _mesh
     * @param isTemperatureMap
     * @param mapRange
     */
    void displayMesh(MyMesh *_mesh, bool isTemperatureMap = false, float mapRange = -1);
    /**
     * @brief Affiche d'un chemin géodésique
     * @param resultpoints vecteur de points du chemin
     */
    void displayPath(vector<CPoint3D> resultpoints);
    void resetAllColorsAndThickness(MyMesh* _mesh);

    /**
     * @brief Point de vue actuel pour les calculs de "visibilité", différent du système de caméra de la fenêtre de visualisation du maillage
     */
    MyMesh::Point camera();
    
     MyMesh::Point creerCamera(float x, float y);

     /**
     * @brief Calcul de la persistance 3D sur le maillage pour tous les (x,y) par pas de @pas.
     * @param pas : le pas pour déterminer les paramètres (x,y) des persistances 2D.
     * @return Les données de la meilleure persistance (caméra, paires et valeur).
     */
    ResultatPersistance persistance3DAll(float pas);

    /**
    * @brief Calcul de la persistance 3D sur le maillage utilisant
    *        un point de départ et ses voisins dont la persistance est plus élevée.
    * @param x : coordonnée x du point de départ.
    * @param y : coordonnée y du point de départ.
    * @param pas : le pas pour déterminer les voisins d'un couple (x,y).
     * @return Les données de la meilleure persistance (caméra, paires et valeur).
    */
    ResultatPersistance persistance3DVoisinsDepuis(float x, float y, float pas, ResultatPersistance resultat_cour);

    /**
    * @brief Calcul de la persistance 3D sur le maillage utilisant
    *        un point de départ et ses voisins dont la persistance est plus élevée.
    * @param pas : le pas pour déterminer les voisins d'un couple (x,y).
     * @return Les données de la meilleure persistance (caméra, paires et valeur).
    */
    ResultatPersistance persistance3DVoisins(float pas);

    /**
     * @brief Calcul de la persistance 2D sur le maillage pour un point de vue donné.
     * @param x : coordonnée x sphérique pour le point de vue.
     * @param y : coordonnée y sphérique pour le point de vue.
     * @return Les données de la persistance (caméra, paires et valeur).
     */
    ResultatPersistance persistance2D(float x, float y);

private slots:
    /**
     * @brief Charge un fichier
     */
    void on_pushButton_chargement_clicked();
    /**
     * @brief Affiche la silouhette
     */
    void on_pushButton_silhouette_clicked();
    /**
     * @brief Affiche le contour
     */
    void on_pushButton_contour_clicked();
    /**
     * @brief Affiche le chemin géodésique entre deux sommets
     */
    void on_pushButton_chemin_clicked();
    /**
     * @brief Calcule la persistance
     */
    void on_pushButton_persistance_clicked();
    /**
     * @brief Calcule la persistance 3D (tous les pas)
     */
    void on_pushButton_persistance3D_tout_clicked();
    /**
     * @brief Calcule la persistance 3D (par voisins)
     */
    void on_pushButton_persistance3D_opti_clicked();
    /**
     * @brief Crée une image du pourcentage de faces cachées
     */
    void on_pushButton_creerImage_clicked();

private:

    /**
     * @brief Maillage utilisant la structure d'OpenMesh
     */
    MyMesh mesh;
    /**
     * @brief Sauvegarde du maillage mesh
     */
    MyMesh ori_mesh;
    /**
     * @brief Maillage utilisant la structure de ChenHanImproved pour le calcul de géodésiques
     */
    CRichModel *model;
    /**
     * @brief Points à utiliser lors du calcul de géodésique
     */
    MyMesh::Point pointStart, pointEnd;
    /**
     * @brief Courbes
     */
    QVector<QVector<VertexHandle>> courbes;
    /**
     * @brief Image contenant le pourcentage de faces cachées selon les coordonnées sphériques
     */
    QLabel *qLabelImg;


    Ui::MainWindow *ui;

    void cacherFaces(MyMesh* _mesh, MyMesh::Point cam);
    /**
     * @brief Affiche le pourcentage de faces rouges
     * @param _mesh
     */
    void pourcentageFaceRouge(MyMesh *_mesh);
    /**
     * @brief Calcule le tour géodésique
     * @param _mesh
     * @param vh1
     * @param vh2
     */
    void calculGeodesiques(int indexV1, int indexV2);
    /**
     * @brief Retourne si une arête fait partie du contour
     * @param _mesh
     * @param eh
     * @return Vrai si l'arête fait partie du contour, faux sinon
     */
    bool estDeContour(MyMesh* _mesh, EdgeHandle eh);
    /**
     * @brief Affiche le pourcentage de faces rouges
     * @param _mesh
     */
    void contour(MyMesh* _mesh);/**
     * @brief Marque un ensemble d'arêtes en changeant leur couleur et leur épaisseur jusqu'à former un contour
     * @param _mesh
     * @param start_eh arête de départ
     * @param tab_passage tableau servant à indiquer quelles arêtes ont été marquées
     */
    void marquerContour(MyMesh* _mesh, EdgeHandle start_eh, bool* tab_passage);
    /**
     * @brief Marque une arête en changeant sa couleur et son épaisseur
     * @param _mesh
     * @param eh arête à marquer
     */
    void marquerArete(MyMesh* _mesh, EdgeHandle eh);
    /**
     * @brief Fonction de test pour afficher les courbes calculées lors du marquage de contour
     */
    void afficherCourbes();
    /**
     * @brief Calcule la distance entre deux sommets du maillage
     * @param vh1
     * @param vh2
     * @return la distance calculée
     */
    float distance(VertexHandle vh1, VertexHandle vh2);
    /**
     * @brief Calcule la distance entre deux points avec la structure CHI
     * @param vh1
     * @param vh2
     * @return la distance calculée
     */
    float distance(CPoint3D p1, CPoint3D p2);
    /**
     * @brief Convertit des courbes en graphe
     * @return Le graphe généré
     */
    Graph toGraph();

    bool areVerticesNeighbors(VertexHandle vh1, VertexHandle vh2);

    /**
     * @brief Marque le sommet @vh avec la couleur @c et une meilleure épaisseur
     * @param vh : sommet à marquer
     * @param c : couleur de marquage
     */
    void marquerSommet(VertexHandle vh, MyMesh::Color c);
    /**
     * @brief Marque les paires 1 et 2 pour les rendre plus visibles
     * @param paire1
     * @param paire2
     */
    void marquerPairesSommets(pair<unsigned,unsigned> paire1 ,pair<unsigned,unsigned> paire2);

    /**
     * @brief Marque les sommets de la persistance et montre l'angle de caméra utilisé avec le contour
     * @param r : Données de la persistance à représenter.
     */
    void marquerPersistance(ResultatPersistance r);

    /**
     * @brief Met à jour les spinBox de recherche de chemin avec les données de la persistance
     * @param resultat
     */
    void mettreSpinAJour(ResultatPersistance resultat);
};


#endif // MAINWINDOW_H
